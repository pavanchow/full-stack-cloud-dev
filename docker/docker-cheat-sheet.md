# Docker Cheat Sheet

- `docker version` - Find the version of the installed docker engine. 
 
- `docker pull image:tag` - Pull the specified image with the specified tag on to the machine. 

- `docker push image:tag` - Push the docker image with the specified tag to the dockerhub.

- `docker build .` - Build an image from a Dockerfile. Also see `docker build --help` and "." in the command is location of the Dockerfile. 

- `docker ps` - List the running containers. Also see `docker ps --help`.

- `docker images` - List all the available images on the machine. Also see `docker images --help`.

- `docker run` - Create a new container.
	- `-d` - run in de-attached mode
	- `-it` - run in interactive mode
	- `--name` - to specifiy a name to the container
	- `-v /home/host/directory:/home/container/directory` - host volumes | to mount file system onto container
	- `-v db-data:/var/lib/mysql/data` - named volume | we specifiy a new name and location of the volume on the machine
	- `docker run -dit -p8003:80 image_name` - start a container 

- `docker exec -it container_ID /bin/bash` - to get into a running container

- `docker image rm` - Removes the docker image

- `docker rmi image_ID` - Removes images using image ID

- `docker start container_ID` - Restart a container

- `docker network ls` - List all the available networks on docker

- `docker network create network_name` - create a custom network

- `docker attach container_ID` - attaches a de-attached container

- `docker create --name image_name -p 80:80 container_ID` - create a image 

- `docker cp File_to_copy container_ID:/directory` - copies file/files into the container

- `docker rm $(docker ps --filter status=exited -q)` - Removes all stopped containers


## docker Composer

- `docker-compose -f mongo-compose.yaml up` = this will create containers with the instructions given by yaml file
	- f = docker-compose yaml file location
	- up = starts the mentioned containers in the file
	- down = stops the mentioned containers in the file