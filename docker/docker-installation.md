# Follow the steps for a cleat docker install


### Update your machine for hassle free installation
- sudo apt update && sudo apt upgrade -y

### Install docker 
- sudo apt install docker.io docker 

### Start the docker service
- systemctl start docker

### Check if docker is working
- docker --version