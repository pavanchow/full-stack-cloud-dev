# Docker 

- Docker is an open source platform for building, deploying, and managing containerized applications.

### Container 

- An isolated environment for running an application. / Container is a software package that consists of all the dependencies required to run an application.

### Some docker tools and terms


- DockerFile
	- Every Docker container starts with a simple text file containing instructions for how to build the Docker container image. DockerFile automates the process of Docker image creation. It’s essentially a list of command-line interface (CLI) instructions that Docker Engine will run in order to assemble the image.
- Docker images
	- Docker images contain executable application source code as well as all the tools, libraries, and dependencies that the application code needs to run as a container. When you run the Docker image, it becomes one instance (or multiple instances) of the container.
- Docker containers
	- Docker containers are the live, running instances of Docker images. While Docker images are read-only files, containers are live, ephemeral, executable content. Users can interact with them, and administrators can adjust their settings and conditions using docker commands.
- Docker Hub
	- [Docker Hub](https://hub.docker.com/) is the public repository of Docker images that calls itself the “world’s largest library and community for container images.” It holds over 100,000 container images sourced from commercial software vendors, open-source projects, and individual developers. It includes images that have been produced by Docker, Inc., certified images belonging to the Docker Trusted Registry, and many thousands of other images.
- Docker daemon
	- Docker daemon is a service running on your operating system. This service creates and manages your Docker images for you using the commands from the client, acting as the control center of your Docker implementation.
- Docker registry
	- A Docker registry is a scalable open-source storage and distribution system for docker images. The registry enables you to track image versions in repositories, using tagging for identification. This is accomplished using git, a version control tool.


### Docker deployment and orchestration

- If you’re running only a few containers, it’s fairly simple to manage your application within Docker Engine, the industry de facto runtime. But if your deployment comprises thousands of containers and hundreds of services, it’s nearly impossible to manage that workflow without the help of these purpose-built tools.

- Docker Compose
	- If you’re building an application out of processes in multiple containers that all reside on the same host, you can use Docker Compose to manage the application’s architecture. Docker Compose creates a YAML file that specifies which services are included in the application and can deploy and run containers with a single command. Using Docker Compose, you can also define persistent volumes for storage, specify base nodes, and document and configure service dependencies.
- Kubernetes
	- To monitor and manage container lifecycles in more complex environments, you’ll need to turn to a container orchestration tool. While Docker includes its own orchestration tool (called Docker Swarm), most developers choose Kubernetes instead.
	- Kubernetes is an open-source container orchestration platform descended from a project developed for internal use at Google. Kubernetes schedules and automates tasks integral to the management of container-based architectures, including container deployment, updates, service discovery, storage provisioning, load balancing, health monitoring, and more. In addition, the open source ecosystem of tools for Kubernetes—including Istio and Knative—enables organizations to deploy a high-productivity Platform-as-a-Service (PaaS) for containerized applications and a faster on-ramp to serverless computing.

### Persisting data with Volumes 

- Data volumes are used for data persistance in docker 
- If we have data on the virtual file system of the container the data would be lost when the container is stopped or restarted. we mount host file system(folder) into the virtual files system of the docker.

- 3 Types of volumes
	- 1. Host volumes
	- 2. Anonymous volumes
	- 3. Named volumes