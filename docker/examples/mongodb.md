# Running mongoDB on docker container


## create a separate network for MongoDB 

`docker network create mongo-network`


## create a mongodb container like the following
##### mongodb = actual DB

`docker run -p27017:27017 -d -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=12345 --name mongodb --net mongo-network mongo`

    - p27018 = port where mongodb runs
    - e = environmental variables
    - name = name of the container
    - net = name of the network


## create a mongo-express for accessing DB with UI

##### mongo-express = UI for DB

`docker run -d \         
-p 8081:8081 \
-e ME_CONFIG_MONGODB_ADMINUSERNAME=admin \
-e ME_CONFIG_MONGODB_ADMINPASSWORD=12345 \
--net mongo-network \
--name mongo-express \
-e ME_CONFIG_MONGODB_SERVER=mongodb \
mongo-express` 

    - ME_CONFIG_MONGODB_SERVER = specify the mongodb server name


### access the DB at http://localhost:8081/